# Getting started using our SDK ![Screenshot](img/favicon_sw.png)

### Requirements

For start to work with superwise, we first need a superwise account.
If you don't have one, please contact us [superwise]("https://www.superwise.ai/").

### Installation
-  Let's install our sdk by the command:
```
    pip3 install superwise
```

- Also, there is an option to download our whl from [superwise-sdk]("")


After install our package, open python shell

### Connection
First we should get a token to working with superwise, for that
follow the syntax bellow:

```python
from superwise.superwise import Superwise

sw = Superwise(<superwise_username>,<superwise_password>)
```

Excepted Output:
```python
 "Connection established.."
```
*Next we will explain what is trace service and how to work with it...*
