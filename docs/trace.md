# Trace Service![Screenshot](img/favicon_sw.png)

Trace service let's you an easy way to send data to superwise.
This data will help us monitor and understand your models behaviors.

First, ensure you create a connection to superwise.

There are two parts of send data to superwise:
1. Send predicted data.
2. Send labled data.


#### Predicted data
There are three ways to send data predicted to superwise:

1. emit request - send single record of data.
2. batch request- send multiple record at once.
3. file request - send a url to a csv with multiple record.

*Each request has a unique format.*

Each requests should have another 2 params:
1. task_id: int - task id reflects a project in superwise.
2. version_id: int - version id reflects a version of the model.

Let's pass on each method!

##### Emit Request
As we mention emit request send single record of data.
The data should be in json format:
```
{
  record: {
    id: string,
    ts: string,
    data: 
        {
        feature_name: feature_value, 
        ..
        },
    prediction_value: string - NOT required 
    prediction_probability: float,
  }
}
```
*Explain about the attributes:*    
* id - id of the sample.
* ts -  timestamp of the sample to be appear.
* data - key value of the features.
* prediction_value
* prediction_probability

*EXAMPLE:*  
```python
from superwise.superwise import Superwise

sw = Superwise(<superwise_username>,<superwise_password>)

my_doc = {
    "id": "1",
    "ts": "1998-04-18 16:48:37.4",
    "data": { "name": "oryan", "age" : 25 },
    "prediction_value": "1",
    "prediction_probability": 0.95,
}
res = sw.trace.emit(data=my_doc,task_id=1,version_id=1)
```
Excepted Response:
```
print(res)
'201'
```

##### Batch Request
Batch request send multiple record of data.
The data should be in this format:
```
{
    records: [{record}]
}
```
Explain about the attributes:



*EXAMPLE:*
```python
from superwise.superwise import Superwise

sw = Superwise(<superwise_username>,<superwise_password>)

my_doc = {
  "records": [
    {
    "id": "1",
    "ts": "1998-04-18 16:48:37.4",
    "data": { "name": "oryan", "age" : 25 }, 
    "prediction_value": "1",
    "prediction_probability": 0.95,
    "version_id": 1
  },
      {
    "id": 2,
    "ts": "1998-04-18 16:48:37.4",
    "data": { "name": "oryan", "age" : 25 }, 
    "prediction_value": "1",
    "prediction_probability": 0.95,
    "version_id": 1
  }]
}
res = sw.trace.batch(data=my_doc,task_id=1,version_id=1)
```
Excepted Response:
```
print(res)
'201'
```
##### File Request
File request send multiple record of data from url store at our object storage.
The file supported just in csv format.
For using that method you should upload your csv file to our bucket. 

*EXAMPLE:*
```python
from superwise.superwise import Superwise

sw = Superwise(<superwise_username>,<superwise_password>)


res = sw.trace.file(url_file="https://superwiseBucket.aws.com/<my file path>",task_id=1,version_id=1)
```
Excepted Response:
```
print(res)
'201'
```
#### Label Data
There are three ways to send label data to superwise:

1. emit label request - send single record of data.
2. batch label request- send multiple record at once.
3. file label request - send a url to a csv with multiple record.


*Next we will explain what is model service and how to work with it...*

