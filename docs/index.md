# Welcome to superwise sdk documentation ![Screenshot](img/favicon_sw.png)

### About us
AI is rapidly transforming businesses and whole industries, greatly increasing autonomous control features with broader and deeper impact on an almost daily basis. But how can one ensure that these systems do what they are expected to do over time? The repercussions of mistakes could be disastrous for any business.


We pioneered the AI Performance Management system to solve this problem and provide peace of mind for enterprises that use AI in production.

### How it works
Our system provides extensive monitoring, comprehensive metric analysis and complete clarity for your data science teams. This allows them to maintain the highest performance levels for their AI models in production.

* *REAL TIME MONITORING*  
Dozens of out-of-the box KPI’s suited for the commonly used AI models are calculated in real time to measure the key statistics for each model

* *PREDICTIVE INTELLIGENCE*  
AI-based automatic identification of issues that might affect the performance before there are any business ramifications

* *AUTOMATIC HANDLING*  
Automatic execution of the necessary corrective actions, taking the maintenance work off the data science team



##### For visiting our website:  [superwise](https://superwise.ai/).


Here we will explain how to easily work with superwise.

*Next we will show how to get started to work with superwise.*

